import 'package:fitness_tracker/widgets/exercise/exercise.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import './workout_viewmodel.dart';

class WorkoutScreen extends StatelessWidget {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    print('Workout screen build');
    return ViewModelBuilder<WorkoutViewModel>.nonReactive(
      viewModelBuilder: () => WorkoutViewModel(),
      onModelReady: (model) => model.init(),
      builder: (
        BuildContext context,
        WorkoutViewModel model,
        Widget child,
      ) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
              backgroundColor: Theme.of(context).bottomAppBarColor,
              leading: IconButton(
                  color: Colors.white,
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    print('back');
                    Navigator.pop(context);
                  })),
          body: Column(children: [
            SizedBox(height: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [Hero(tag: 'push', child: Text(model.workout.name))],
            ),
            SizedBox(height: 20),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 6),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        topLeft: Radius.circular(50)),
                    color: Theme.of(context).cardColor,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0, 4.0),
                          blurRadius: 6.0,
                          spreadRadius: 1)
                    ]),
                child: FormBuilder(
                  key: _fbKey,
                  child: ListView(children: [
                    SizedBox(height: 20),
                    for (var x in model.workout.exercises)
                      Exercise(
                        name: x.name,
                        numberOfsets: x.numberOfSets,
                        volume: x.volume,
                      ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                          child: Text('Finish'),
                          onPressed: () {
                            _fbKey.currentState.save();
                            model.textFieldData = _fbKey.currentState.value;
                            model.createExercises(model.textFieldData);
                            print(model.textFieldData);
                          }),
                    )
                  ]),
                ),
              ),
            )
          ]),
        );
      },
    );
  }
}
