import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fitness_tracker/data_models/workout.dm.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class DatabaseService {
  final _db = FirebaseFirestore.instance;

  final StreamController<String> _streamController =
      StreamController<String>.broadcast();

  void init() {}

  Future addWorkout(WorkoutDataModel workout) async {
    try {
      _db.collection('workouts').add(workout.toJson());
    } catch (e) {
      print(e.message);
    }
  }

  Stream listenToWorkoutsRealTime() {
    return _db.collection('workouts').snapshots();
  }

  Stream listenToLogsRealTime() {
    return _db.collection('logs').snapshots();
  }
}
