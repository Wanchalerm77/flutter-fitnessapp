import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class ExpansionPanelViewModel extends BaseViewModel {
  List<ExpansionPanel> categories;
  var exercises = [
    {
      'name': 'Chest',
      'exercises': ['Bench Press', 'Incline Press']
    },
    {
      'name': 'Back',
      'exercises': ['Pull Ups', 'Row']
    },
    {
      'name': 'Legs',
      'exercises': ['Squat', 'Lunges']
    },
  ];

  List<ExpansionPanel> buildCategories() {
    categories = exercises
        .map((e) => ExpansionPanel(
              headerBuilder: (BuildContext context, bool isExpanded) =>
                  ListTile(
                title: Text(e['name']),
              ),
              body: Column(
                children: List.from((e['exercises']))
                    .map((e) => ListTile(
                          title: Text(e),
                        ))
                    .toList(),
              ),
            ))
        .toList();

    return categories;
  }
}

class Category {
  Widget body;
  String header;
  bool isExpanded;

  Category({this.header, this.body, this.isExpanded});

  List<Category> buildCategories(Map<String, dynamic> json) {
    return List.generate(
        json.length,
        (index) => Category(
            header: json[index].name,
            body: buildBody(json[index].exercises),
            isExpanded: false));
  }

  Widget buildBody(List exercises) {
    return Column(
      children: exercises.map((e) => ListTile(title: Text(e))).toList(),
    );
  }
}
