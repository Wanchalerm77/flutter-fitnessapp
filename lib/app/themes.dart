import 'package:flutter/material.dart';

/**
 * AppBar: #212121
 * 1. background #303030 
 * 2. surface/cards #424242 
 * 3. Primary color
 * 4. secondary color
 * 5.Colors for text on background, surface, primary and secondary
 * ==> #FFFFFF
 * 
 */

ThemeData get darkTheme => ThemeData(
    bottomAppBarColor: Color(0xff212121),
    backgroundColor: Color(0xff303030),
    accentColor: Colors.red,
    cardColor: Color(0xff424242),
    buttonTheme: ButtonThemeData(
        textTheme: ButtonTextTheme.primary, buttonColor: Colors.green),
    iconTheme: IconThemeData(color: Colors.white),
    textTheme: TextTheme(
            subtitle1: TextStyle(),
            caption: TextStyle(),
            button: TextStyle(),
            bodyText2: TextStyle())
        .apply(bodyColor: Colors.white, displayColor: Colors.white),
    primaryColor: Color(0xffFFCA33));
