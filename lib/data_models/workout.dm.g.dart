// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workout.dm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkoutDataModel _$WorkoutDataModelFromJson(Map<String, dynamic> json) {
  return WorkoutDataModel(
    name: json['name'] as String,
    subInfo: json['subInfo'] as String,
    exercises: (json['exercises'] as List)
        ?.map((e) => e == null
            ? null
            : ExerciseDataModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$WorkoutDataModelToJson(WorkoutDataModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'subInfo': instance.subInfo,
      'exercises': instance.exercises?.map((e) => e?.toJson())?.toList(),
    };
