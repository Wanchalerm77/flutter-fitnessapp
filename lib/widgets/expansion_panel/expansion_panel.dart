import 'dart:convert';

import 'package:fitness_tracker/widgets/expansion_panel/expansion_panel_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class ExpansionPanelView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ExpansionPanelViewModel>.reactive(
      viewModelBuilder: () => ExpansionPanelViewModel(),
      builder: (
        BuildContext context,
        ExpansionPanelViewModel model,
        Widget child,
      ) {
        return Expanded(
          child: ListView(
            children: [
              ExpansionPanelList(
                  expandedHeaderPadding: EdgeInsets.zero,
                  expansionCallback: (int index, bool isExpanded) {
                  },
                  children: model.buildCategories())
            ],
          ),
        );
      },
    );
  }
}
