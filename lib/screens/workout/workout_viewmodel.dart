import 'package:fitness_tracker/app/locator.dart';
import 'package:fitness_tracker/data_models/exercises.dm.dart';
import 'package:fitness_tracker/data_models/workout.dm.dart';
import 'package:logger/logger.dart';
import 'package:stacked/stacked.dart';

class WorkoutViewModel extends BaseViewModel {
  ExerciseDataModel tmpEx;
  var textFieldData;

  var _log = Logger();

  WorkoutDataModel workout = WorkoutDataModel.fromJson({
    'name': ' Push Workout',
    'subInfo': 'heavy',
    'exercises': [
      {
        'name': 'bench press',
        'numberOfSets': 3,
        'volume': {
          'bench press weight0': 100,
          'bench press weight1': 110,
          'bench press weight2': 120,
          'bench press reps0': 10,
          'bench press reps1': 8,
          'bench press reps2': 6
        }
      },
      {
        'name': 'squat',
        'numberOfSets': 3,
        'volume': {
          'squat weight0': 120,
          'squat weight1': 130,
          'squat weight2': 140,
          'squat reps0': 10,
          'squat reps1': 8,
          'squat reps2': 6
        }
      }
    ]
  });

  void init() {}

  void createExercises(Map<String, dynamic> textfieldData) {
    textfieldData.forEach((key, value) {
      for (int i = 0; i < workout.exercises.length; i++) {
        if (key.contains(workout.exercises[i].name)) {
          workout.exercises[i].volume[key] = 999;
        }
      }
    });
    _log.i(workout.toJson());
  }
}
