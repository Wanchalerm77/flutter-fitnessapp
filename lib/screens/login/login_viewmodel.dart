import 'package:fitness_tracker/app/locator.dart';
import 'package:fitness_tracker/app/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class LoginViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();

  Future navigateToHome() async {
    print('navigated');
    final homePage = Routes.homeScreen;
    await _navigationService.navigateTo(homePage);
  }
}
