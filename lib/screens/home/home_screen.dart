import 'package:fitness_tracker/screens/create_workout/create_workout_screen.dart';
import 'package:fitness_tracker/screens/explore/explore_screen.dart';
import 'package:fitness_tracker/screens/logs_history/logs_screen.dart';
import 'package:fitness_tracker/screens/workout/workout_screen.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import 'home_viewmodel.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //To-DO implement animation

    return ViewModelBuilder<HomeViewModel>.nonReactive(
        onModelReady: (model) => model.init(),
        builder: (context, model, child) => Scaffold(
            backgroundColor: Theme.of(context).backgroundColor,
            floatingActionButton: (model.currentIndex == 1)
                ? FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (ctx) => CreateWorkoutView()));
                    },
                  )
                : null,
            body: SafeArea(
              child: PageView(
                controller: model.pc,
                onPageChanged: (index) => model.setIndex(index),
                children: [LogsScreen(), ExploreScreen(), LogsScreen()],
              ),
            ),
            bottomNavigationBar: CurvedNavigationBar(
                backgroundColor: Theme.of(context).backgroundColor,
                color: Theme.of(context).bottomAppBarColor,
                animationCurve: Curves.easeInOut,
                animationDuration: Duration(milliseconds: 200),
                index: model.currentIndex,
                height: 50,
                items: <Widget>[
                  Icon(Icons.add, size: 30),
                  Icon(Icons.list, size: 30),
                  Icon(Icons.compare_arrows, size: 30),
                ],
                onTap: (index) {
                  //Handle button tap
                  if (model.pc.hasClients) {
                    print(model.currentIndex);
                    model.pc.animateToPage(index,
                        duration: model.pageTransitionDuration,
                        curve: model.pageCurveAnimation);
                  }
                })),
        viewModelBuilder: () => HomeViewModel());
  }
}
