import 'package:injectable/injectable.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:logger/logger.dart';

@module
abstract class ThirdPartyModules {
  @lazySingleton
  NavigationService get navigationService;

  @lazySingleton
  Logger get loggerService;
}
