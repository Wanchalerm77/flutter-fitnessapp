// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercises.dm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExerciseDataModel _$ExerciseDataModelFromJson(Map<String, dynamic> json) {
  return ExerciseDataModel(
    name: json['name'] as String,
    numberOfSets: json['numberOfSets'] as int,
    volume: json['volume'] as Map<String, dynamic>,
  );
}

Map<String, dynamic> _$ExerciseDataModelToJson(ExerciseDataModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'numberOfSets': instance.numberOfSets,
      'volume': instance.volume,
    };
