import 'package:fitness_tracker/screens/home/home_viewmodel.dart';
import 'package:fitness_tracker/screens/workout/workout_screen.dart';
import 'package:fitness_tracker/widgets/exercise/exercise.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class Workout extends ViewModelWidget<HomeViewModel> {
  final String name;
  final String subInfo;
  final List<Exercise> exercises;

  Workout({@required this.name, this.subInfo, this.exercises}) {}

  @override
  Widget build(BuildContext context, HomeViewModel model) {
    return Card(
        elevation: 1,
        margin: EdgeInsets.only(bottom: 10),
        child: ListTile(
          title: Text(this.name),
          subtitle: this.subInfo != null ? Text(this.subInfo) : SizedBox(),
          trailing: Hero(
              tag: this.name,
              child: RaisedButton(
                  child: Text('GO'),
                  onPressed: () {
                    // ExtendedNavigator.root.push(Routes.workoutScreen);
                    model.addWorkout(name: 'bench 2');
                  })),
          onTap: () {
            if (model.pc.hasClients) {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return WorkoutScreen();
              }));
            }
          },
        ));
  }
}
