import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'exercises.dm.g.dart';

@JsonSerializable()
class ExerciseDataModel {
  final String name;
  int numberOfSets;
  Map<String, dynamic> volume;

  ExerciseDataModel({@required this.name, this.numberOfSets = 3, this.volume});

  factory ExerciseDataModel.fromJson(Map<String, dynamic> data) =>
      _$ExerciseDataModelFromJson(data);

  Map<String, dynamic> toJson() => _$ExerciseDataModelToJson(this);
}
