import 'package:fitness_tracker/widgets/log/log_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class Log extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<LogViewModel>.nonReactive(
      viewModelBuilder: () => LogViewModel(),
      builder: (
        BuildContext context,
        LogViewModel model,
        Widget child,
      ) {
        return Card(
          child: Container(
              child: ListTile(                
            title: Text('Push Workout'),
            subtitle: Text('84m'),
            trailing: Text('19-09-2020'),
          )),
        );
      },
    );
  }
}
