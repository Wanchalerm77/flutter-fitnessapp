import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class SetWidget extends StatelessWidget {
  String exerciseName;
  int setNumber;

  TextEditingController _tcWeight;
  TextEditingController _tcReps;

  SetWidget(String exerciseName, int setNumber, int weight, int reps) {
    this.exerciseName = exerciseName;
    this.setNumber = setNumber;
    this._tcWeight = TextEditingController(text: '$weight');
    this._tcReps = TextEditingController(text: '$reps');
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(width: 50),
        Container(width: 100, child: Text('$setNumber')),
        Container(
            width: 50,
            child: FormBuilderTextField(
                controller: _tcWeight,
                attribute: '$exerciseName weight$setNumber',
                keyboardType: TextInputType.number)),
        SizedBox(width: 100),
        Container(
            width: 50,
            child: FormBuilderTextField(
                controller: _tcReps,
                attribute: '$exerciseName reps$setNumber',
                keyboardType: TextInputType.number))
      ],
    );
  }
}
