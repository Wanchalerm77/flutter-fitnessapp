import 'package:fitness_tracker/screens/explore/explore_viewmodel.dart';
import 'package:fitness_tracker/widgets/workout/workout.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class ExploreScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('explorescreen');
    return ViewModelBuilder.reactive(
        builder: (context, model, child) => Column(
              children: [
                SizedBox(height: 50),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('My Workouts'),
                      Icon(
                        Icons.search,
                      )
                    ],
                  ),
                ),
                SizedBox(height: 50),
                Expanded(
                    child: StreamBuilder(
                        stream: model.stream,
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Text('No workouts');
                          } else {
                            return ListView.builder(
                                itemCount: snapshot.data.docs.length,
                                itemBuilder: (context, index) {
                                  return Workout(
                                      name: snapshot.data.docs[index]
                                          .data()['name']);
                                });
                          }
                        }))
              ],
            ),
        viewModelBuilder: () => ExploreViewModel());
  }
}
