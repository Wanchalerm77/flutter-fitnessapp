import 'package:fitness_tracker/data_models/exercises.dm.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'workout.dm.g.dart';

@JsonSerializable(explicitToJson: true)
class WorkoutDataModel {
  final String name;
  final String subInfo;
  List<ExerciseDataModel> exercises;

  WorkoutDataModel(
      {@required this.name, this.subInfo, @required this.exercises});

  factory WorkoutDataModel.fromJson(Map<String, dynamic> data) =>
      _$WorkoutDataModelFromJson(data);

  Map<String, dynamic> toJson() => _$WorkoutDataModelToJson(this);
}
