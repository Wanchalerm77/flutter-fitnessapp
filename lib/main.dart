import 'package:fitness_tracker/app/locator.dart';
import 'package:fitness_tracker/app/themes.dart';
import 'package:fitness_tracker/screens/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseFirestore.instance.settings = Settings(
      host: '10.0.2.2:8080', sslEnabled: false, persistenceEnabled: false);
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    print('Main App');
    return MaterialApp(
        title: 'Flutter Demo', theme: darkTheme, home: LoginScreen()

        // builder: ExtendedNavigator.builder<CustomRouter>(
        //     router: CustomRouter(),
        //     navigatorKey: locator<NavigationService>().navigatorKey));
        );
  }
}
