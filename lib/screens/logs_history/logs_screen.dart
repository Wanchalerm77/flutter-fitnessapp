import 'package:fitness_tracker/screens/logs_history/logs_screen_viewmodel.dart';
import 'package:fitness_tracker/widgets/log/log.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class LogsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('logs screen');
    return ViewModelBuilder<LogsScreenViewModel>.nonReactive(
      viewModelBuilder: () => LogsScreenViewModel(),
      builder: (
        BuildContext context,
        LogsScreenViewModel model,
        Widget child,
      ) {
        return Column(children: [
          SizedBox(height: 50),
          Container(
            height: 100,
            child: Center(
              child: Text('Logs'),
            ),
          ),
          Expanded(
            child: ListView(
                children: [Log(), Log(), Log(), Log(), Log(), Log(), Log()]),
          )
        ]);
      },
    );
  }
}
