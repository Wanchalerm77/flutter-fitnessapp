import 'package:injectable/injectable.dart';
import 'package:get_it/get_it.dart';
// Important. Impore the locator.iconfig.dart file
import './locator.config.dart';

final locator = GetIt.instance;

@injectableInit
void setupLocator() => $initGetIt(locator);
