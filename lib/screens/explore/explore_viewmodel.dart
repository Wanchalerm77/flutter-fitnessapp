import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fitness_tracker/app/locator.dart';
import 'package:fitness_tracker/services/db_service.dart';
import 'package:stacked/stacked.dart';


class ExploreViewModel extends StreamViewModel<QuerySnapshot> {
  Stream<QuerySnapshot> get stream =>
      locator<DatabaseService>().listenToWorkoutsRealTime();
}
