import 'package:stacked/stacked.dart';

class ExerciseViewModel extends BaseViewModel {
  String name;
  int numberOfSets = 3;

  void addSet(String exerciseName) {
    this.numberOfSets++;

    notifyListeners();
  }
}
