// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';
import 'package:stacked_services/stacked_services.dart';

import '../services/db_service.dart';
import '../services/third_party_modules.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final thirdPartyModules = _$ThirdPartyModules(get);
  gh.lazySingleton<DatabaseService>(() => DatabaseService());
  gh.lazySingleton<Logger>(() => thirdPartyModules.loggerService);
  gh.lazySingleton<NavigationService>(
      () => thirdPartyModules.navigationService);
  return get;
}

class _$ThirdPartyModules extends ThirdPartyModules {
  final GetIt _get;
  _$ThirdPartyModules(this._get);
  @override
  Logger get loggerService => Logger(
        filter: _get<LogFilter>(),
        printer: _get<LogPrinter>(),
        output: _get<LogOutput>(),
        level: _get<Level>(),
      );
  @override
  NavigationService get navigationService => NavigationService();
}
