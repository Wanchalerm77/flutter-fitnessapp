import 'package:fitness_tracker/app/locator.dart';
import 'package:fitness_tracker/data_models/workout.dm.dart';
import 'package:fitness_tracker/services/db_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';

class HomeViewModel extends IndexTrackingViewModel {
  String _welcomeMessage = 'Welcome User';
  String get welcomeMessage => _welcomeMessage;

  PageController _pc = PageController(initialPage: 1);
  PageController get pc => _pc;

  Curve pageCurveAnimation = Curves.easeInOut;
  Duration pageTransitionDuration = Duration(milliseconds: 250);

  final _dbService = locator<DatabaseService>();
  DatabaseService get dbService => _dbService;

  void init() {
    _dbService.init();
    this.setIndex(1);
  }

  Future addWorkout({@required String name}) {
    setBusy(true);
    var result = _dbService.addWorkout(WorkoutDataModel(name: name));
    setBusy(false);
  }
}
