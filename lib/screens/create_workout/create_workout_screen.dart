import 'package:fitness_tracker/screens/create_workout/create_workout_viewmodel.dart';
import 'package:fitness_tracker/widgets/expansion_panel/expansion_panel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class CreateWorkoutView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('create workout screen');
    return ViewModelBuilder<CreateWorkoutViewModel>.reactive(
        viewModelBuilder: () => CreateWorkoutViewModel(),
        builder: (
          BuildContext context,
          CreateWorkoutViewModel model,
          Widget child,
        ) {
          return Scaffold(
              body: FormBuilder(
                  child: Column(children: [
            FormBuilderTextField(
              decoration: InputDecoration(labelText: 'Name'),
            ),
            ExpansionPanelView()
          ])));
        });
  }
}
