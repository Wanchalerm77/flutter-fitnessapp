import 'package:fitness_tracker/widgets/exercise/exercise_viewmodel.dart';
import 'package:fitness_tracker/widgets/set/set.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class Exercise extends StatelessWidget {
  final String name;
  int numberOfsets;
  Map<String, dynamic> volume;

  Exercise({@required this.name, this.numberOfsets = 3, this.volume});

  @override
  Widget build(BuildContext context) {
    print(' exercise build');
    print(this.volume);
    return ViewModelBuilder.reactive(
      viewModelBuilder: () => ExerciseViewModel(),
      builder: (context, model, child) => Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text(this.name)],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              SizedBox(width: 50),
              Container(width: 50, child: Text('Sets')),
              SizedBox(width: 50),
              Container(width: 50, child: Text('Weight')),
              SizedBox(width: 100),
              Container(width: 50, child: Text('Reps')),
            ],
          ),
          for (int i = 0; i < this.numberOfsets; i++)
            SetWidget(this.name, i, this.volume['$name weight$i'],
                this.volume['$name reps$i']),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.only(right: 20),
                padding: const EdgeInsets.all(8.0),
                child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                        onTap: () {},
                        splashColor: Colors.red[200],
                        child: RaisedButton(
                          onPressed: () {
                            model.addSet();
                            print(model.sets.length);
                          },
                          elevation: 2.0,
                          child: Icon(
                            Icons.add,
                            size: 15.0,
                          ),
                          padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(),
                        ))),
              )
            ],
          ),
          Divider(
            thickness: 0.5,
            color: Colors.black,
          ),
        ],
      ),
    );
  }
}
