import 'package:auto_route/auto_route.dart';
import 'package:auto_route/auto_route_annotations.dart';
import 'package:fitness_tracker/app/router.gr.dart';
import 'package:fitness_tracker/screens/home/home_screen.dart';
import 'package:fitness_tracker/screens/login/login_screen.dart';
import 'package:fitness_tracker/screens/workout/workout_screen.dart';

//tested keybard rebuilt bug
@CupertinoAutoRouter(routes: <AutoRoute>[
  // initial route is named "/"
  CustomRoute(page: LoginScreen, initial: true),
  CustomRoute(page: HomeScreen),
  CustomRoute(
      page: WorkoutScreen, transitionsBuilder: TransitionsBuilders.slideBottom)
])
class $CustomRouter {
  void navigateToHomeScreen() {
    ExtendedNavigator.root.push(Routes.homeScreen);
  }
}
